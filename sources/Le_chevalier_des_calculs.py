import pyxel
import random

class Personnage:
    def __init__(self):
        self.x = 0
        self.y = 104  # Position verticale initiale
        self.vitesse = 2
        self.en_mouvement = False
        self.direction = 1
        self.jump_speed = 10  # Vitesse initiale du saut
        self.gravity = 2  # Gravité
        self.is_jumping = False  # Indicateur de saut
    
    def update(self):
        self.en_mouvement = False
        if pyxel.btn(pyxel.KEY_RIGHT) or pyxel.btn(pyxel.KEY_L):
            self.x = min(144, self.x + self.vitesse)
            self.en_mouvement = True
            self.direction = 1
        elif pyxel.btn(pyxel.KEY_LEFT) or pyxel.btn(pyxel.KEY_J):
            self.x = max(0, self.x - self.vitesse)
            self.en_mouvement = True
            self.direction = -1

        # Saut
        if pyxel.btn(pyxel.KEY_SPACE) and not self.is_jumping:
            self.is_jumping = True
            self.jump_speed = 10  # Réinitialiser la vitesse de saut

        if self.is_jumping:
            self.y -= self.jump_speed
            self.jump_speed -= self.gravity
            if self.jump_speed < -10:  # Valeur arbitraire pour la hauteur maximale du saut
                self.jump_speed = 10
                self.is_jumping = False

        # Gérer les limites du saut pour empêcher le personnage de passer sous la ligne de sol
        if self.y > 104:
            self.y = 104
            self.jump_speed = 0
            self.is_jumping = False

    def draw(self):
        if self.en_mouvement:
            if pyxel.frame_count % 6 < 3:
                pyxel.blt(self.x, self.y, 1, 0, 0, self.direction * 20, 16, 12)
            else:
                pyxel.blt(self.x, self.y, 2, 0, 0, self.direction * 20, 16, 12)
        else:
            pyxel.blt(self.x, self.y, 1, 0, 0, self.direction * 20, 16, 12)
        
        if pyxel.btnp(pyxel.KEY_B):
            pyxel.image(1).load(0, 0, "PERSOOOONOIR.png")
            pyxel.image(2).load(0, 0, "persomarchenoir.png")
        
        elif pyxel.btnp(pyxel.KEY_A):
            pyxel.image(1).load(0, 0, "PERSOOOO.png")
            pyxel.image(2).load(0, 0, "persomarche.png")
        

class Nuage:
    def __init__(self, x, y, index):
        self.x = x
        self.y = y
        self.replaced = False
        self.vitesse = 2
    
    def update(self):
        if pyxel.frame_count % 10 == 0:
            self.x = self.x - self.vitesse
    
    def draw(self):
        pyxel.blt(self.x, self.y, 0, 0, 16, 160, 17, 12)

class Foret:
    def __init__(self):
        self.x = 0
        self.vitesse = 1
        self.direction = 1
    
    def update(self):
        if pyxel.btn(pyxel.KEY_RIGHT) or pyxel.btn(pyxel.KEY_L):
            self.x = min(240, self.x - self.vitesse)
            self.direction = 1
        elif pyxel.btn(pyxel.KEY_LEFT) or pyxel.btn(pyxel.KEY_J):
            self.x = max(0, self.x + self.vitesse)
            self.direction = - 1
    
    def draw(self):
        k = 0
        while self.x - k*160 > -160:
            pyxel.image(0).load(0, 0, "buisson2.jpg")
            pyxel.blt(self.x - k*160, pyxel.height-10,0,0,48,160,16)
            k = k + 1
        k = 1
        while self.x + k*160 < pyxel.width:
            pyxel.blt(self.x + k*160, pyxel.height-10,0,0,48,160,16)
            k = k + 1


class Oiseau:
    def __init__(self):
        self.x = 160
        self.y = random.randint(pyxel.height // 1.3, pyxel.height - 16)
        self.vitesse = random.randint(3, 7)
        self.direction = -1
    
    def update(self):
        self.x += self.direction * self.vitesse

    def draw(self):
        pyxel.blt(self.x, self.y, 0, -3, 16, 16, 16, 12)



class App:
    def __init__(self):
        pyxel.init(160, 120, title="DODGE or DIE")

        self.characters = [
            {"index": 1, "x": 20, "y": 40, "width": 48, "height": 60},
            {"index": 2, "x": 100, "y": 40, "width": 48, "height": 60}
            ]
        
        self.selected_character = 0
        self.current_page = 0
        
        self.nuages = []
        premier_nuage = Nuage(pyxel.width/2 - 80, pyxel.height//4, 0)
        self.nuages.append(premier_nuage)
        
        self.oiseaux = []

        self.personnage = Personnage()
        self.foret = Foret()
        self.score = 0
        pyxel.run(self.update, self.draw)

    def update(self):
        self.personnage.update()
        self.foret.update()
        
        if self.current_page == 2:
            if pyxel.frame_count % 60 == 0:
                self.oiseaux.append(Oiseau())
                self.score += 1

        for nuage in self.nuages:
            nuage.update()
            if nuage.x < 0 and not nuage.replaced:
                nouveau_nuage = Nuage(pyxel.width, pyxel.height//4, 0)
                self.nuages.append(nouveau_nuage)
                nuage.replaced = True
            if nuage.x < -160:
                self.nuages.remove(nuage)

        for oiseau in self.oiseaux:
            oiseau.update()

            # Gestion des collisions avec les oiseaux
            if (
                oiseau.x < self.personnage.x + 8 and
                oiseau.x + 9 > self.personnage.x and
                oiseau.y < self.personnage.y + 8 and
                oiseau.y + 8 > self.personnage.y
            ):
                self.change_page()
                self.score = 0

            if oiseau.x < -16:
                self.oiseaux.remove(oiseau)

        if pyxel.btnp(pyxel.KEY_Q):
                    pyxel.quit()

        elif pyxel.btnp(pyxel.KEY_A) or pyxel.btnp(pyxel.KEY_B) or pyxel.btnp(pyxel.KEY_SPACE):
            if self.current_page == 2:
                self.next_page()

            else:
                self.change_page()

        
        elif pyxel.btnp(pyxel.KEY_LEFT) or pyxel.btnp(pyxel.KEY_J):
            self.selected_character = (self.selected_character + 1) % len(self.characters)

        elif pyxel.btnp(pyxel.KEY_RIGHT) or pyxel.btnp(pyxel.KEY_L):
            self.selected_character = (self.selected_character - 1) % len(self.characters)


    def draw(self):
        pyxel.cls(0)

        if self.current_page == 0:
            pyxel.text(55, 40, "DODGE or DIE", pyxel.frame_count % 16)
            pyxel.text(28, 70, "APPUYER SUR ESPACE POUR JOUER", pyxel.frame_count % 16)


        elif self.current_page == 1:
            pyxel.text(39 , 12, "CHOISI TON CHEVALIER", pyxel.frame_count % 16)
            pyxel.text(101, 30, "CHEVALIER B", pyxel.frame_count % 16)
            pyxel.text(22, 30, "CHEVALIER A", pyxel.frame_count % 16)            

            for character in self.characters:
                pyxel.blt(
                    character["x"], character["y"],
                    character["index"], 0, 0, character["width"], character["height"]
                )
            
            pyxel.rectb( 
                self.characters[self.selected_character]["x"] - 2,
                self.characters[self.selected_character]["y"] - 2,
                self.characters[self.selected_character]["width"] + 4,
                self.characters[self.selected_character]["height"] + 4, 9
            ) 
        
        elif self.current_page == 2:
            pyxel.cls(12)
            self.foret.draw()
            self.personnage.draw()
            for nuage in self.nuages:
                nuage.draw()
            
            for oiseau in self.oiseaux:
                oiseau.draw()
            pyxel.text(115, 2, "Score : " + str(self.score), 7)


    def change_page(self):
        self.current_page = (self.current_page + 1) % 3
        pyxel.image(1).load(0, 0, "chevalier1.png")
        pyxel.image(2).load(0, 0, "chevalier2.png")
        if self.current_page == 2:
            self.current_page -2

    def next_page(self):
        pass

App()